package com.atlassian.json.jsonpatch;
import java.io.IOException;
import java.text.ParseException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonPatchOperation
{
    public enum OpType {
        test, remove, add, replace, move, copy;

        public static OpType fromString(String op)
        {
            return valueOf(op);
        }
    };

    private final JsonNode node;

    public JsonPatchOperation(JsonNode node)
    {
        this.node = node;
        getOperation();
        getPath();
    }

    static JsonPatchOperation parse(String s) throws JsonParseException, JsonMappingException, IOException
    {
        ObjectMapper mapper = new ObjectMapper();

        JsonNode node = mapper.readValue(s, JsonNode.class);

        return new JsonPatchOperation(node);
    }

    @Override
    public int hashCode()
    {
        return node.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof JsonPatchOperation)
        {
            return node.equals(((JsonPatchOperation) obj).node);
        }
        else
        {
            return false;
        }
    }

    public OpType getOperation()
    {
        return OpType.fromString(node.path("op").asText());
    }

    public JsonPointer getPath()
    {
        JsonNode p = node.path("path");
        if (!p.isMissingNode())
        {
            try
            {
                return new JsonPointer(p.asText());
            }
            catch (ParseException e)
            {
                throw new IllegalArgumentException("Unable to parse path", e);
            }
        }
        else
        {
            throw new IllegalArgumentException();
        }
    }

    JsonNode getValue()
    {
        return node.get("value");
    }

    JsonPointer getFrom() throws JsonPatchFailedException
    {
        try
        {
            return new JsonPointer(node.get("from").asText());
        }
        catch (ParseException pe)
        {
            throw new JsonPatchFailedException("Unable to parse 'from'", pe);
        }
    }

    static PathOperations getPathOperations(JsonPointer path, JsonNode doc)
    {
        return path.resolve(doc, locationResolver);
    }

    public void apply(JsonNode doc) throws JsonPatchFailedException
    {
        PathOperations path = getPathOperations(getPath(), doc);

        switch (getOperation())
        {
            case test:
                if (!getValue().equals(path.get()))
                {
                    throw new JsonPatchFailedException("test: expected '" + getValue() + "' for '" + getPath() + "' but was '" + path.get() + "'");
                }
                break;

            case add:
                path.add(getValue());
                break;

            case remove:
                path.remove();
                break;

            case replace:
                path.replace(getValue());
                break;

            case move:
                path.moveFrom(doc, getFrom());
                break;

            case copy:
                path.copyFrom(doc, getFrom());
                break;

            default:
                throw new IllegalStateException();
        }
    }

    private static final JsonPointer.Resolver<PathOperations> locationResolver = new JsonPointer.Resolver<PathOperations>() {
        @Override
        public PathOperations resolveToRootNode(JsonNode node)
        {
            return new NodeLocation(node);
        }

        @Override
        public PathOperations resolveToObjectMember(ObjectNode node, String field)
        {
            return new ObjectLocation(node, field);
        }

        @Override
        public PathOperations resolveToArrayItem(ArrayNode node, int index)
        {
            return new ArrayLocation(node, index);
        }
    };

    static interface PathOperations
    {
        JsonNode get() throws JsonPatchFailedException;

        void add(JsonNode value) throws JsonPatchFailedException;

        void remove() throws JsonPatchFailedException;

        void replace(JsonNode value) throws JsonPatchFailedException;

        void moveFrom(JsonNode doc, JsonPointer from) throws JsonPatchFailedException;

        void copyFrom(JsonNode doc, JsonPointer from) throws JsonPatchFailedException;
    }

    static class NodeLocation implements PathOperations
    {
        private final JsonNode location;

        public NodeLocation(JsonNode location)
        {
            this.location = location;
        }

        public JsonNode get()
        {
            return location;
        }

        @Override
        public void add(JsonNode value) throws JsonPatchFailedException
        {
            throw new JsonPatchFailedException();
        }

        @Override
        public void remove() throws JsonPatchFailedException
        {
            throw new JsonPatchFailedException();
        }

        @Override
        public void replace(JsonNode value) throws JsonPatchFailedException
        {
            throw new JsonPatchFailedException();
        }

        @Override
        public void moveFrom(JsonNode doc, JsonPointer from) throws JsonPatchFailedException
        {
            throw new JsonPatchFailedException();
        }

        @Override
        public void copyFrom(JsonNode doc, JsonPointer from) throws JsonPatchFailedException
        {
            throw new JsonPatchFailedException();
        }
    }

    static class ObjectLocation implements PathOperations
    {
        private final ObjectNode node;
        private final String token;

        public ObjectLocation(ObjectNode node, String token)
        {
            this.node = node;
            this.token = token;
        }

        @Override
        public JsonNode get() throws JsonPatchFailedException
        {
            if (!node.has(token))
            {
                throw new JsonPatchFailedException();
            }

            return node.get(token);
        }

        @Override
        public void add(JsonNode value) throws JsonPatchFailedException
        {
            if (node.has(token))
            {
                throw new JsonPatchFailedException();
            }

            node.put(token, value);
        }

        @Override
        public void remove() throws JsonPatchFailedException
        {
            if (!node.has(token))
            {
                throw new JsonPatchFailedException();
            }
            node.remove(token);
        }

        @Override
        public void replace(JsonNode value) throws JsonPatchFailedException
        {
            if (!node.has(token))
            {
                throw new JsonPatchFailedException();
            }
            node.put(token, value);
        }

        @Override
        public void moveFrom(JsonNode doc, JsonPointer from) throws JsonPatchFailedException
        {
            PathOperations ops = getPathOperations(from, doc);

            JsonNode value = ops.get();

            if (value.isMissingNode())
            {
                throw new JsonPatchFailedException();
            }

            ops.remove();

            add(value);
        }

        @Override
        public void copyFrom(JsonNode doc, JsonPointer from) throws JsonPatchFailedException
        {
            PathOperations ops = getPathOperations(from, doc);

            JsonNode value = ops.get();

            if (value.isMissingNode())
            {
                throw new JsonPatchFailedException();
            }

            add(value);
        }
    }

    static class ArrayLocation implements PathOperations
    {
        private final ArrayNode a;
        private final int index;

        public ArrayLocation(ArrayNode a, int index)
        {
            this.a = a;
            this.index = index;
        }

        @Override
        public JsonNode get() throws JsonPatchFailedException
        {
            if (index < 0 || index > a.size())
            {
                throw new JsonPatchFailedException();
            }

            return a.get(index);
        }

        @Override
        public void add(JsonNode value) throws JsonPatchFailedException
        {
            if (index > a.size())
            {
                throw new JsonPatchFailedException();
            }

            a.insert(index, value);
        }

        @Override
        public void remove() throws JsonPatchFailedException
        {
            if (index >= a.size())
            {
                throw new JsonPatchFailedException();
            }

            a.remove(index);
        }

        @Override
        public void replace(JsonNode value) throws JsonPatchFailedException
        {
            if (index >= a.size())
            {
                throw new JsonPatchFailedException();
            }

            a.set(index, value);
        }

        @Override
        public void moveFrom(JsonNode doc, JsonPointer from) throws JsonPatchFailedException
        {
            PathOperations ops = getPathOperations(from, doc);

            JsonNode value = ops.get();

            if (value.isMissingNode())
            {
                throw new JsonPatchFailedException();
            }

            ops.remove();

            add(value);
        }

        @Override
        public void copyFrom(JsonNode doc, JsonPointer from) throws JsonPatchFailedException
        {
            PathOperations ops = getPathOperations(from, doc);

            JsonNode value = ops.get();

            if (value.isMissingNode())
            {
                throw new JsonPatchFailedException();
            }

            add(value);
        }
    }
}
