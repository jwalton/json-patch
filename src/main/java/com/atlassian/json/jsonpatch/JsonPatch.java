package com.atlassian.json.jsonpatch;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;


public class JsonPatch
{
    private final List<JsonPatchOperation> operations;

    public JsonPatch(List<JsonPatchOperation> ops)
    {
        this.operations = ops;
    }

    public static JsonPatch parse(InputStream in) throws JsonParseException, JsonMappingException, IOException
    {
        ObjectMapper mapper = new ObjectMapper();

        JsonNode rootNode = mapper.readValue(in, JsonNode.class);

        if (rootNode instanceof ArrayNode)
        {
            List<JsonPatchOperation> ops = new ArrayList<JsonPatchOperation>();

            for (JsonNode n : (ArrayNode) rootNode)
            {
                ops.add(new JsonPatchOperation(n));
            }

            return new JsonPatch(ops);
        }
        else
        {
            throw new IllegalArgumentException("Unexpected type: " + rootNode);
        }
    }

    public JsonNode apply(JsonNode nodeBefore) throws JsonPatchFailedException
    {
        JsonNode node = nodeBefore.deepCopy();

        for (JsonPatchOperation op : operations)
        {
            op.apply(node);
        }

        return node;
    }

    public List<JsonPatchOperation> getOperations()
    {
        return operations;
    }
}
