package com.atlassian.json.jsonpatch;

public class JsonPatchFailedException extends Exception
{
    JsonPatchFailedException()
    {
    }

    JsonPatchFailedException(String message)
    {
        super(message);
    }

    JsonPatchFailedException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
