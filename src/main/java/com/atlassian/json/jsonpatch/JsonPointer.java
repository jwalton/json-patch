package com.atlassian.json.jsonpatch;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonPointer
{
    private final List<String> tokens;

    public JsonPointer(String pointer) throws ParseException
    {
        List<String> tokens = new ArrayList<String>();

        char[] ca = pointer.toCharArray();

        int i = 0;

        while (i < ca.length)
        {
            if (ca[i] != '/')
            {
                throw new ParseException("JSON Pointer tokens must start with '/'", i);
            }
            i++;

            StringBuilder sb = new StringBuilder();

            while (i < ca.length && ca[i] != '/')
            {
                if (ca[i] == '~')
                {
                    i++;
                    if (i >= ca.length)
                    {
                        throw new ParseException("Incomplete escape sequence", i);
                    }

                    if (ca[i] == '0')
                    {
                        sb.append('~');
                    }
                    else if (ca[i] == '1')
                    {
                        sb.append('/');
                    }
                    else
                    {
                        throw new ParseException("Unexpected escape character: " + ca[i], i);
                    }
                }
                else
                {
                    sb.append(ca[i]);
                }

                i++;
            }

            tokens.add(sb.toString());
        }

        this.tokens = tokens;
    }

    public List<String> getTokens()
    {
        return tokens;
    }

    public JsonNode evaluate(JsonNode node)
    {
        return resolve(node, getTokens(), getResolver);
    }

    public <T> T resolve(JsonNode node, Resolver<T> resolver)
    {
        return resolve(node, getTokens(), resolver);
    }

    private static int indexOf(ArrayNode a, String token)
    {
        if (token.equals("-"))
        {
            return a.size();
        }
        else
        {
            try
            {
                return Integer.parseInt(token);
            }
            catch (NumberFormatException e)
            {
                throw new IllegalArgumentException("Bad array index: " + token);
            }
        }
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        for (String s : tokens)
        {
            sb.append('/');
            for (char c : s.toCharArray())
            {
                switch (c)
                {
                    case '~':
                        sb.append("~0");
                        break;

                    case '/':
                        sb.append("~1");
                        break;

                    default:
                        sb.append(c);
                }
            }
        }

        return sb.toString();
    }

    private static <T> T resolve(JsonNode node, List<String> tokens, Resolver<T> resolver)
    {
        if (tokens.isEmpty())
        {
            return resolver.resolveToRootNode(node);
        }

        String head = tokens.get(0);
        List<String> tail = tokens.subList(1, tokens.size());

        if (tail.isEmpty())
        {
            if (node instanceof ObjectNode)
            {
                return resolver.resolveToObjectMember((ObjectNode) node, head);
            }
            else if (node instanceof ArrayNode)
            {
                ArrayNode a = (ArrayNode) node;

                return resolver.resolveToArrayItem(a, indexOf(a, head));
            }
            else
            {
                throw new IllegalArgumentException("Unable to extract '" + head + "' from a JSON node of type " + node.getClass());
            }
        }

        if (node instanceof ObjectNode)
        {
            return resolve(((ObjectNode) node).get(head), tail, resolver);
        }
        else if (node instanceof ArrayNode)
        {
            ArrayNode a = (ArrayNode) node;

            return resolve(a.get(indexOf(a, head)), tail, resolver);
        }
        else
        {
            throw new IllegalArgumentException("Unable to extract '" + head + "' from a JSON node of type " + node.getClass());
        }
    }

    static interface Resolver<T>
    {
        T resolveToRootNode(JsonNode node);
        T resolveToObjectMember(ObjectNode node, String field);
        T resolveToArrayItem(ArrayNode node, int index);
    }

    private static final Resolver<JsonNode> getResolver = new Resolver<JsonNode>() {
        @Override
        public JsonNode resolveToRootNode(JsonNode node)
        {
            return node;
        }

        @Override
        public JsonNode resolveToObjectMember(ObjectNode node, String field)
        {
            return node.get(field);
        }

        @Override
        public JsonNode resolveToArrayItem(ArrayNode node, int index)
        {
            return node.get(index);
        }
    };
}
