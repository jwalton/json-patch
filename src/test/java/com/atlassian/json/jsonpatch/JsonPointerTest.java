package com.atlassian.json.jsonpatch;
import java.io.InputStream;
import java.text.ParseException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.TextNode;

import org.junit.Test;


import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class JsonPointerTest
{
    @Test
    public void emptyStringIsAcceptable() throws ParseException
    {
        assertEquals(emptyList(), new JsonPointer("").getTokens());
    }

    @Test(expected = ParseException.class)
    public void tokensMustStartWithSlash() throws ParseException
    {
        new JsonPointer("a");
    }

    @Test
    public void tokensAreParsed() throws ParseException
    {
        assertEquals(asList("a", "b"), new JsonPointer("/a/b").getTokens());
    }

    @Test
    public void emptyTokensArePermitted() throws ParseException
    {
        assertEquals(asList("", ""), new JsonPointer("//").getTokens());
    }

    @Test
    public void escapedTokensAreRecognised() throws ParseException
    {
        assertEquals(asList("~"), new JsonPointer("/~0").getTokens());
        assertEquals(asList("/"), new JsonPointer("/~1").getTokens());
    }

    @Test
    public void escapesWithinTokensAreAccepted() throws ParseException
    {
        assertEquals(asList("a~"), new JsonPointer("/a~0").getTokens());
        assertEquals(asList("a/"), new JsonPointer("/a~1").getTokens());
        assertEquals(asList("~a"), new JsonPointer("/~0a").getTokens());
        assertEquals(asList("/a"), new JsonPointer("/~1a").getTokens());
    }

    @Test(expected = ParseException.class)
    public void unknownEscapeCausesParsingToFail() throws ParseException
    {
        new JsonPointer("/~2");
    }

    @Test(expected = ParseException.class)
    public void incompleteEscapeCausesParsingToFail() throws ParseException
    {
        new JsonPointer("/~2");
    }

    @Test(expected = ParseException.class)
    public void incompleteEscapeWithinTokenCausesFailure() throws ParseException
    {
        new JsonPointer("/a~");
    }

    @Test
    public void examplesFromSpec() throws ParseException
    {
        assertEquals(asList("foo"), new JsonPointer("/foo").getTokens());
        assertEquals(asList("foo", "0"), new JsonPointer("/foo/0").getTokens());
        assertEquals(asList(""), new JsonPointer("/").getTokens());
        assertEquals(asList("a/b"), new JsonPointer("/a~1b").getTokens());
        assertEquals(asList("c%d"), new JsonPointer("/c%d").getTokens());
        assertEquals(asList("e^f"), new JsonPointer("/e^f").getTokens());
        assertEquals(asList("g|h"), new JsonPointer("/g|h").getTokens());
        assertEquals(asList("i\\j"), new JsonPointer("/i\\j").getTokens());
        assertEquals(asList("k\"l"), new JsonPointer("/k\"l").getTokens());
        assertEquals(asList(" "), new JsonPointer("/ ").getTokens());
        assertEquals(asList("m~n"), new JsonPointer("/m~0n").getTokens());
    }

    @Test
    public void evaluatingYieldsResultsFromDocuments() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("json-pointer-sample-document.json");
        assertNotNull(in);

        ObjectMapper mapper = new ObjectMapper();

        JsonNode node = mapper.readValue(in, JsonNode.class);

        assertEquals(node, new JsonPointer("").evaluate(node));
        assertEquals(mapper.readValue("[\"bar\", \"baz\"]", JsonNode.class), new JsonPointer("/foo").evaluate(node));
        assertEquals(new TextNode("bar"), new JsonPointer("/foo/0").evaluate(node));
        assertEquals(new IntNode(0), new JsonPointer("/").evaluate(node));
        assertEquals(new IntNode(1), new JsonPointer("/a~1b").evaluate(node));
        assertEquals(new IntNode(2), new JsonPointer("/c%d").evaluate(node));
        assertEquals(new IntNode(3), new JsonPointer("/e^f").evaluate(node));
        assertEquals(new IntNode(4), new JsonPointer("/g|h").evaluate(node));
        assertEquals(new IntNode(5), new JsonPointer("/i\\j").evaluate(node));
        assertEquals(new IntNode(6), new JsonPointer("/k\"l").evaluate(node));
        assertEquals(new IntNode(7), new JsonPointer("/ ").evaluate(node));
        assertEquals(new IntNode(8), new JsonPointer("/m~0n").evaluate(node));
    }

    @Test(expected = IllegalArgumentException.class)
    public void unableToGetNonIntegerItemFromList() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("json-pointer-sample-document.json");
        assertNotNull(in);

        ObjectMapper mapper = new ObjectMapper();

        JsonNode node = mapper.readValue(in, JsonNode.class);

        new JsonPointer("/foo/list-element").evaluate(node);
    }

    @Test(expected = IllegalArgumentException.class)
    public void unableToGetItemFromTerminalNode() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("json-pointer-sample-document.json");
        assertNotNull(in);

        ObjectMapper mapper = new ObjectMapper();

        JsonNode node = mapper.readValue(in, JsonNode.class);

        new JsonPointer("/foo/0/element-of-string").evaluate(node);
    }

    @Test
    public void toStringReturnsOriginalPath() throws ParseException
    {
        assertEquals("", new JsonPointer("").toString());
        assertEquals("/", new JsonPointer("/").toString());
        assertEquals("/test", new JsonPointer("/test").toString());
    }

    @Test
    public void toStringReturnsOriginalPathWithEscaping() throws ParseException
    {
        assertEquals("/a~0", new JsonPointer("/a~0").toString());
        assertEquals("/a~1", new JsonPointer("/a~1").toString());
        assertEquals("/~1a", new JsonPointer("/~1a").toString());
    }
}
