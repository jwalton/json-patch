package com.atlassian.json.jsonpatch;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class AppendixAExamplesTest
{
    private final String before, patch, after;

    static NodeList loadExampleSections() throws ParserConfigurationException, XPathExpressionException, SAXException, IOException
    {
        URL url = AppendixAExamplesTest.class.getResource("draft-ietf-appsawg-json-patch-10.xml");
        assertNotNull(url);

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        dbf.setAttribute("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

        DocumentBuilder db = dbf.newDocumentBuilder();

        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();

        return (NodeList) xpath.evaluate("//section[@title='Examples']/section", db.parse(new InputSource(url.toString())), XPathConstants.NODESET);
    }

    @Parameters(name = "{0}")
    public static Iterable<Object[]> data() throws SAXException, IOException, ParserConfigurationException, XPathExpressionException
    {
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();

        NodeList nodes = loadExampleSections();

        Collection<Object[]> tests = new ArrayList<Object[]>();

        for (int i = 0; i < nodes.getLength(); i++)
        {
            Element section = (Element) nodes.item(i);

            String title = section.getAttribute("title");

            NodeList figures = (NodeList) xpath.evaluate("figure/artwork", section, XPathConstants.NODESET);

            if (figures.getLength() != 3)
            {
//                System.err.println("Skipping : " + title);
                continue;
            }

            assertEquals(3, figures.getLength());

            String[] args = {
                    title,
                    figures.item(0).getTextContent(),
                    figures.item(1).getTextContent(),
                    figures.item(2).getTextContent()
            };

            tests.add(args);
        }

        /* A test missing from the spec */
        String[] args = {
                "Copying an array element",
                "{\"foo\": [ \"all\", \"grass\", \"cows\", \"eat\" ]}",
                "[{ \"op\": \"copy\", \"path\": \"/foo/-\", \"from\": \"/foo/1\" }]",
                "{\"foo\": [ \"all\", \"grass\", \"cows\", \"eat\", \"grass\" ]}",
        };

        tests.add(args);

        return tests;
    }

    public AppendixAExamplesTest(String name, String before, String patch, String after)
    {
        this.before = before;
        this.patch = patch;
        this.after = after;
    }

    @Test
    public void patchCanBeParsed() throws Exception
    {
        JsonPatch patchObj = JsonPatch.parse(new ByteArrayInputStream(patch.getBytes("utf-8")));
        assertNotNull(patchObj);
    }

    @Test
    public void patchProducesExpectedResults() throws Exception
    {
        JsonPatch patchObj = JsonPatch.parse(new ByteArrayInputStream(patch.getBytes("utf-8")));
        assertNotNull(patchObj);

        ObjectMapper mapper = new ObjectMapper();

        JsonNode nodeBefore = mapper.readValue(before, JsonNode.class);
        JsonNode nodeExpectedAfter = mapper.readValue(after, JsonNode.class);

        JsonNode nodeAfter = patchObj.apply(nodeBefore);

        assertEquals(nodeExpectedAfter, nodeAfter);

        JsonNode nodeBeforeUnchanged = mapper.readValue(before, JsonNode.class);
        assertEquals("The source node should not have been changed",
                nodeBeforeUnchanged, nodeBefore);
    }
}
