package com.atlassian.json.jsonpatch;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.atlassian.json.jsonpatch.JsonPatchOperation.OpType;

import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;


public class JsonPatchTest
{
    @Test
    public void sampleDocumentIsParsed() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("document-structure-sample.json-patch");
        assertNotNull(in);

        JsonPatch patch = JsonPatch.parse(in);
        assertNotNull(patch);
    }

    @Test
    public void patchOperationsAreCounted() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("document-structure-sample.json-patch");
        assertNotNull(in);

        JsonPatch patch = JsonPatch.parse(in);
        assertThat(patch.getOperations(), IsCollectionWithSize.hasSize(6));
    }

    @Test
    public void patchOperationsHaveExpectedTypes() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("document-structure-sample.json-patch");
        assertNotNull(in);

        JsonPatch patch = JsonPatch.parse(in);

        List<JsonPatchOperation.OpType> expectedTypes = Arrays.asList(
                JsonPatchOperation.OpType.test,
                JsonPatchOperation.OpType.remove,
                JsonPatchOperation.OpType.add,
                JsonPatchOperation.OpType.replace,
                JsonPatchOperation.OpType.move,
                JsonPatchOperation.OpType.copy);

        List<JsonPatchOperation.OpType> types = new ArrayList<JsonPatchOperation.OpType>();

        for (JsonPatchOperation op : patch.getOperations())
        {
            types.add(op.getOperation());
        }

        assertEquals(expectedTypes, types);
    }
}
