package com.atlassian.json.jsonpatch;
import java.util.HashSet;
import java.util.Set;

import com.atlassian.json.jsonpatch.JsonPatchOperation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


public class JsonPatchOperationTest
{
    @Test
    public void operationsAreEquivalent() throws Exception
    {
        String[] equivalent = {
                "{ \"op\": \"add\", \"path\": \"/a/b/c\", \"value\": \"foo\" }",
                "{ \"path\": \"/a/b/c\", \"op\": \"add\", \"value\": \"foo\" }",
                "{ \"value\": \"foo\", \"path\": \"/a/b/c\", \"op\": \"add\" }"
        };

        Set<JsonPatchOperation> distinct = new HashSet<JsonPatchOperation>();
        for (String s : equivalent)
        {
            distinct.add(JsonPatchOperation.parse(s));
        }

        assertThat(distinct, IsCollectionWithSize.hasSize(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void patchOperationRequiresOpMember()
    {
        JsonNode node = new ObjectNode(JsonNodeFactory.instance);

        new JsonPatchOperation(node);
    }

    @Test(expected = IllegalArgumentException.class)
    public void patchOperationRequiresPathMember()
    {
        ObjectNode node = new ObjectNode(JsonNodeFactory.instance);
        node.put("op", "test");

        new JsonPatchOperation(node);
    }

    @Test
    public void minimalPatchOperationOnlyRequiresOpAndPath()
    {
        ObjectNode node = new ObjectNode(JsonNodeFactory.instance);
        node.put("op", "test");
        node.put("path", "/");

        JsonPatchOperation op = new JsonPatchOperation(node);
        assertEquals(JsonPatchOperation.OpType.test, op.getOperation());
    }

    @Test
    public void operationsAreRecognisedFromLowercaseStrings()
    {
        assertEquals(JsonPatchOperation.OpType.test, JsonPatchOperation.OpType.fromString("test"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void operationsAreNotRecognisedFromMixedCaseStrings()
    {
        JsonPatchOperation.OpType.fromString("Test");
    }
}
