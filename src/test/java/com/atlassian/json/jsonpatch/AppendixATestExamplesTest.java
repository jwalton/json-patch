package com.atlassian.json.jsonpatch;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import com.atlassian.json.jsonpatch.JsonPatch;
import com.atlassian.json.jsonpatch.JsonPatchFailedException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(Parameterized.class)
public class AppendixATestExamplesTest
{
    @Parameters(name = "{0}")
    public static Iterable<Object[]> data() throws SAXException, IOException, ParserConfigurationException, XPathExpressionException
    {
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();

        NodeList nodes = AppendixAExamplesTest.loadExampleSections();

        Collection<Object[]> tests = new ArrayList<Object[]>();

        for (int i = 0; i < nodes.getLength(); i++)
        {
            Element section = (Element) nodes.item(i);

            String title = section.getAttribute("title");

            NodeList figures = (NodeList) xpath.evaluate("figure/artwork", section, XPathConstants.NODESET);

            if (figures.getLength() != 2 || !title.startsWith("Testing a Value: " ))
            {
//                System.err.println("Skipping : " + title);
                continue;
            }

            assertEquals(2, figures.getLength());

            Object[] args = {
                    title,
                    figures.item(0).getTextContent(),
                    figures.item(1).getTextContent(),
                    title.endsWith("Success")
            };

            tests.add(args);
        }

        return tests;
    }

    private final String document;
    private final String patch;
    private final boolean success;

    public AppendixATestExamplesTest(String name, String document, String patch, boolean success)
    {
        this.document = document;
        this.patch = patch;
        this.success = success;
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testingAValueSuccess() throws Exception
    {
        JsonPatch patchObj = JsonPatch.parse(new ByteArrayInputStream(patch.getBytes("utf-8")));
        assertNotNull(patchObj);

        ObjectMapper mapper = new ObjectMapper();

        JsonNode doc = mapper.readValue(document, JsonNode.class);

        if (!success)
        {
            thrown.expect(JsonPatchFailedException.class);
        }

        JsonNode nodeAfter = patchObj.apply(doc);

        assertEquals(doc, nodeAfter);
    }
}
